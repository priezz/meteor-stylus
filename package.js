Package.describe({
  summary: 'Expressive, dynamic, robust CSS',
  name: 'priezz:stylus',
  version: "2.513.5",
  git: "https://gitlab.com/priezz/meteor-stylus.git"
});

Package.registerBuildPlugin({
  name: 'compileStylusBatch',
  use: ['ecmascript', 'caching-compiler', 'stylus@2.513.5'],
  sources: [
    'plugin/compile-stylus.js'
  ],
  npmDependencies: {
    // 'stylus': '0.53.0',
    //'stylus': "https://github.com/meteor/stylus/tarball/d4352c9cb4056faf238e6bd9f9f2172472b67c5b", // fork of 0.51.1
    'nib': "1.1.2",
    'jeet': '6.1.2',
    'rupture': '0.6.1'
  }
});

Package.onUse(function (api) {
  //api.use('isobuild:compiler-plugin@1.0.0');
  api.use('isobuild:compiler-plugin');
});

Package.onTest(function (api) {
  api.use(['tinytest', 'stylus@2.513.5', 'test-helpers', 'templating']);
  api.addFiles([
    'stylus_tests.html',
    'stylus_tests.styl',
    'stylus_tests.import.styl',
    'stylus_tests.js'
  ],'client');
});
